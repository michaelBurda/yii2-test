<?php 
	use yii\helpers\Url; 
?>

<?php if (Yii::$app->session->hasFlash('errorDownload')): ?>
	<strong class="label label-danger">Sorry, something went wrong...</strong>
<?php else: ?>
	<a href="<?= Url::toRoute(["site/download", "file" => "test.7z"]) ?>">Download archive</a>
<?php endif; ?>